import sys
import pdb
import argparse
import clusterpy

def csv(value):
    return value.split(',')

basic = argparse.ArgumentParser(add_help=False)
# required parameters
basic.add_argument('inputFile',
                   type=str,
                   help='input file to be clustered')
basic.add_argument('variables',
                   #nargs='+',
                   type=csv,
                   help='variables to be used')
basic.add_argument('nRegions',
                   type=int,
                   help='number of regions')
basic.add_argument('outputFile',
                   type=str,
                   help='output file with the result')
# optional parameters
arisel = argparse.ArgumentParser(description='Execute Arisel to a shape file of polygons',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 parents=[basic])
arisel.add_argument('-wtype',
                    type=str,
                    help='contiguity type to be used',
                    default="rook",
                    choices=["rook","queen"],
                    dest="wType")
arisel.add_argument('-inits',
                    type=int,
                    help='number of initial feasible solutions to be constructed before applying Tabu Search',
                    default=5,
                    dest="inits")
arisel.add_argument('-std',
                    type=int,
                    help='If std is 1, then the variables will be standardized',
                    choices=[0,1],
                    default=0,
                    dest="std")
arisel.add_argument('-convTabu',
                    type=int,
                    help='Stop the search after convTabu nonimproving moves (nonimproving moves are those moves that do not improve the current solution. Note that "improving moves" are different to "aspirational moves"). If convTabu=0 the algorithm will stop after Int(M/N) nonimproving moves.',
                    default=0,
                    dest="convTabu")
arisel.add_argument('-tabuLength',
                    type=int,
                    help='Number of times a reverse move is prohibited.',
                    default=10,
                    dest="tabuLength")
args = arisel.parse_args()


# runing the algorithm
layer = clusterpy.importArcData(args.inputFile)
print args.inits
#pdb.set_trace()
layer.cluster('arisel',
              args.variables,
              args.nRegions,
              wType=args.wType,
              inits=args.inits,
              dissolve=1,
              std=args.std,
              convTabu=args.convTabu,
              tabuLength=args.tabuLength)
layer.results[0].exportArcData(args.outputFile)
