# encoding: latin2
"""Toolboxes modulule
"""
__author__ = "Juan C. Duque, Alejandro Betancourt"
__credits__= "Copyright (c) 2009-10 Juan C. Duque"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "RiSE Group"
__email__ = "contacto@rise-group.org"

import cluster as clus
from cluster import * 
__all__ = clus.__all__
